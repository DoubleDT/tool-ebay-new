// Basic init
const { app, BrowserWindow, ipcMain } = require("electron");
const tool = require("./be/tool");
const upload = require("./be/upload");
const fs = require('fs');
const path = require('path');
const os = require('os');
const fileDir = path.join(os.homedir(), 'Desktop', 'result')
// Let electron reloads by itself when webpack watches changes in ./app/
//require("electron-reload")(__dirname);

// To avoid being garbage collected
let mainWindow;

app.on("ready", () => {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 587,
    webPreferences: {
      nodeIntegration: true
    }
  });
  fs.mkdir(fileDir, () => { })
  // mainWindow.webContents.openDevTools();
  mainWindow.loadURL(`file://${__dirname}/app/index.html`);
});
// mongoose.connect(
//   "mongodb+srv://buithehoan:colevay2011@dungpt-brji5.mongodb.net/tool_ebay?retryWrites=true",
//   { useNewUrlParser: true },
//   err => {
//     if (err) console.log(err);
//     console.log("Mongodb connected");

//   }
// );

ipcMain.on("add-links", async (event, { links }) => {
  const regex = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
  links = links.split("\n");
  links = links.filter(x => regex.test(x));

  //loop start here
  let finalArray = []
  for (let i = 0; i < links.length; i++) {
    let re = await action(links[i])
    finalArray.push(re)
  }
  // let url = links[0]
  async function action(url) {
    mainWindow.webContents.send("noti", `[${url}] Loading url...`);
    let arrayLink = await tool(url)
    mainWindow.webContents.send("noti", `[${url}] Image uloading...`);
    let arr2 = []
    for (let item of arrayLink) {
      let { title, name, link } = item;
      if (fs.existsSync(link)) {
        let result = await upload({ title, name, link })
        arr2.push(result)
      }
    }

    mainWindow.webContents.send("noti", `[${url}]  Image uploaded...`);
    let text = arr2.map(item => {
      return `${item.title}|${item.name}|${item.linkDrive}`
    })
    text = text.join("\n")
    return text;
  }
  finalArray = finalArray.join("\n")

  let fileName = `result_${new Date().getDate()}_${new Date().getMonth() + 1}_${new Date().getTime()}.txt`
  fs.writeFile(`${fileDir}/${fileName}`, finalArray, () => {
    mainWindow.webContents.send("noti", `Completed! Result stored in ${fileDir}/${fileName}`);
    setTimeout(() => {
      mainWindow.webContents.send("success", `${fileDir}/${fileName}`);
    }, 2000)
  })
  // loop end
  // let mpLinks = [];
  // links.map(x => {
  //   if (x != "") {
  //     mpLinks.push({ link: x });
  //   }

  // });
  // try {
  //   let r = await Links.insertMany(mpLinks);
  //   console.log(r)
  // } catch (error) {
  //   console.log(error)
  //   console.log("erevything ok");
  // }
  // return event.sender.send("message", `Updated!`);
});
// ipcMain.on("export-file", async (event, { store }) => {
//   let data = await Data.find({ isPrint: false });
//   if (!fs.existsSync(store)) {
//     return event.sender.send("message", `Incorrect folder`);
//   }
//   if (data.length === 0) {
//     return event.sender.send("message", `Empty`);;
//   }
//   await Data.updateMany({ isPrint: false }, { isPrint: true })
//   let ArrayData = []
//   data.map(item => {
//     let line = `${item.title}|${item.name}|${item.linkDrive}`
//     ArrayData.push(line);
//   })
//   let name = `result_${new Date().getDate()}_${new Date().getMonth()}_${new Date().getTime()}.txt`
//   let file = fs.createWriteStream(`${store}/${name}`);
//   file.on('error', function (err) {
//     return event.sender.send("message", "Export fail. Please try again");
//   });
//   let listPromises = ArrayData.map(x => {
//     return new Promise(resolve => {
//       file.write(x + '\r\n');
//     })
//   })
//   await Promise.all(listPromises);
//   return event.sender.send("message", `Export sucess! Check result here: ${store}/${name}`);
// })
//cron job
// setInterval(async () => {
//   let con = { isUsed: false }
//   let length = await Links.countDocuments(con);
//   if (length === 0) {
//     return;
//   }
//   let skip = Math.floor(Math.random() * length);
//   let linkEbay = await Links.findOne(con)
//     .skip(skip)
//     .lean();
//   if (!linkEbay) return;
//   console.log('vao day 1 lan')
//   await Links.findOneAndUpdate({ _id: linkEbay._id }, { isUsed: true })
//   mainWindow.webContents.send("message", `Loading new link!`);
//   let arrayLink = await tool(linkEbay.link);
//   let arrayResult = [];
//   uploadImage();
//   let i = 0;
//   function uploadImage() {
//     setTimeout(async () => {
//       let { title, name, link } = arrayLink[i];
//       let check = await Data.findOne({ title });
//       if (!check && fs.existsSync(link)) {
//         let result = null
//         try {
//           result = await upload({ title, name, link });
//         } catch (error) {
//           result = null;
//         }
//         arrayResult.push(result);
//       }

//       i++;
//       if (i < arrayLink.length) {
//         uploadImage();
//       }
//       if (i == arrayLink.length) {
//         return cb();
//       }
//     }, 1000);
//   }
//   async function cb() {
//     let finalResults = await Promise.all(arrayResult);
//     finalResults = finalResults.filter(x => x !== null)
//     try {
//       await Data.insertMany(finalResults);
//     } catch (error) {
//       console.log("everything ok 2");
//     }
//     setTimeout(() => {
//       mainWindow.webContents.send("message", `Free!`);
//     }, 5000);
//     return mainWindow.webContents.send("message", `Action completed!!`);
//   }
// }, 1000);
