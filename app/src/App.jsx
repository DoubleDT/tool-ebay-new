import React, {Component} from 'react'
import {render} from 'react-dom'
import Main from './components/Main'

import ElectronImg from './assets/electron.png'
import ReactImg from './assets/react.png'
import WebpackImg from './assets/webpack.png'

const logos = [
    ElectronImg,
    ReactImg,
    WebpackImg
]


export default class App extends Component {
    render() {
        return (
            <div>
                <Main></Main>
            </div>
        )
    }
}
