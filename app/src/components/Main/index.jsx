import React, { Component } from 'react';
import styles from './style.css'
import { ipcRenderer } from 'electron';
import moment from 'moment-timezone'
export default class Main extends Component {
  constructor(props) {
    super(props)
    this.formSubmit = this.formSubmit.bind(this)
    this.linksChange = this.linksChange.bind(this)
    this.state = {
      links: "",
      message: [
        `[${moment().format('DD/MM/YYYY-HH:mm:ss')}] Tool ready`
      ],
      store: "",
      isProcessing: false
    }
  }
  componentDidMount() {
    ipcRenderer.on('noti', (event, message) => {
      this.setState({
        message: [...this.state.message, `[${moment().format('DD/MM/YYYY-HH:mm:ss')}]${message}`]
      })
    })
    ipcRenderer.on('success', (event, message) => {
      this.setState({ isProcessing: false })
      this.setState({ links: "" })
      alert(`Success! You file have stored in: ${message}`)
    })
  }
  formSubmit(e) {
    e.preventDefault()
    if (this.state.links === '') {
      return alert("This field can't empty")
    }
    this.setState({ isProcessing: true })
    ipcRenderer.send('add-links', { links: this.state.links })
  }
  linksChange(e) {
    this.setState({ links: e.target.value })
  }
  render() {
    return (
      <div className="container">
        <div className="form-submit">
          <form action="" className={styles.form} onSubmit={this.formSubmit}>
            <textarea name="links" id="links" cols="30" rows="15" onChange={(e) => this.linksChange(e)} value={this.state.links} ></textarea>
            <button type="submit" disabled={this.state.isProcessing} className={styles.buttonSubmit}>Submit</button>
          </form>
        </div>
        <div className={styles.showInfo}>
          {
            this.state.message.map(item => {
              return (
                <p style={{ margin: '5px' }}>{item}</p>
              )
            })
          }
        </div>
      </div>
    )
  }
}