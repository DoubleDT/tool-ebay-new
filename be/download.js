const axios = require('axios');
const fs = require('fs')
var randomstring = require("randomstring");
const path = require('path')
const appDir = path.dirname(require.main.filename);
let link = 'https://i.ebayimg.com/thumbs/images/m/mejgKYcMa9v_M_hJ-afX6_Q/s-l225.jpg'


module.exports = async (link) => {
  return new Promise(async resolve => {
    let name = `${randomstring.generate()}.jpg`
    let store = `${appDir}/be/images/${name}`
    let download = fs.createWriteStream(store)
    link = link.replace('225', '1000')
    let config = {
      method: 'get',
      url: link,
      responseType: 'stream'
    }
    await axios(config)
      .then(async res => {
        if (res.status === 200) {
          await res.data.pipe(download)
          return resolve({ error: false, name: name, link: store })
        } else {
          return resolve({ error: true })
        }
      })
  })
}
