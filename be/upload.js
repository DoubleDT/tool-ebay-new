const { google } = require("googleapis");
const fs = require("fs");
const privateKey =
  "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCdzYLpWojQpDQv\nEPx+D2CFNZElLKVwpu4Wzg8BUQS14Wq6Vt0atk+O7HJ95qPfMBUtQFfvfU/6BdWQ\n3It63Un66BGQA9kNhcCQ6cFRUPzV90WPCDVCLKsKcnvmL3v+6Y+b8/430oMV/jGo\nTi+AcBHi0navuBCsKghaGyOpS1JRWcvkxTxe1BV55rq6DrwwEb8kHqaGkwOwl+z+\n2y1lpSvzDcjcBAw9THJKdFbKkxpgfxw1GPrP3hsAeo+pdsQKXgKkByaauASqKoPy\nIlmtEcc7rtEz/K5Q2P6wRbNfI8jkO+z+pJFZs9dWgaw9vU/Q3cfDw2ZppqM1S/aI\nO+uElFN1AgMBAAECggEAAK+tGxCuuaGNLIs5Xd75P3sNJ4k7KvM+49B/ljTg/V1+\nllLcYUpbrDZxBQUkv0q5PNojx8HWwDKYTCn5XU5rDPFHakXqAroLLVYm5hChGNkb\nb1TMzn3dPI1k5opcUq53VmlIOLvsoALb+mxknn1eRos9wuPdBrKkGKg3jnvqXrow\nfEOaQeEoERtVEPyJ94BIfeZ8oiSW1D4u0srwZNshrOxZnTxUqTw4LdVNwOvNTmnc\n/zMIT0cz/4Hqq/QukbHxf3PjbqlcBMmXXyGSYiulWd7VBNYERLtWK7QxnCoLwO6U\nlnpryZOj+Fm5qSsqgzz5F5+IFAcrg9/nq8Ajin8rPQKBgQDdAVlgwlbigbv6QNhD\n3Y0Hyez7PVHpoh7i9emNx4G8EdMmZxoeUgV6QkxGURKpTnziTPKULWXl5PyOi95L\nvdLhGhjh14GdD4ZzI6FD3Z3ZSylv9E5ach/l7YsQb9S/XqvsUC+V2g/17q20qeUX\nqb6zsGaun8BV8z7P7Ql5Z/XvnwKBgQC2yjEA55HoLbQHJgfNnqUPNy5+tNgIPhGu\nb46mFu32kUMiYTOtoqBr27V1xkB2RCh7bCpvVo5qXSQE0yw18HZMTIDJNXoPcawB\nySpkMG/eNJ+HRe/OQeKA67iO2g9V5Hn6V1G2Ve+9e9zwfKVOPvASpFFPFuz/+AST\nmng/PTdUawKBgEvIr6ZfXb3UMOpym/ZBnZ/DHVFVyKXt4tt+6fSlKhacoiQ0DJ/D\nDn/LBi7ClDycXnSXYENxgdSgff4/dh3Q3OQ1t7yR0q9R6I/I8v67muotPOtYAho/\nsjJ9D0e2oWRyCVBCet7O5NQ3YTdnXtiLa4pgfgV5YFHY1HwmUy5feNUPAoGAWKyT\nwz2Twc/SIxmm9XA94LjDXIJ8vtrllvrVd+ttu4zSxa3j9RsMueZXDHCFuxnrjZx2\npZaW9jCDFbggHXau3OrMG1BPC4j4idE+OGZrDp/2uBgMcsHxkNtJqDtO7zsslW6g\n3FSqPgEFuMCvLNtJebGvJDAZ/z8hcT2hkgQvgEsCgYEAmBvlZjZY/0bX5fmX7FWT\nfqNzIUjRSOhIKm9DWXRYrTCszYivANQKbjYHQuNCIIw1ujf5oUR1S42oGwmmwLPx\n/WbqBu1aZF+jaFzJnbM4NxTgUQpynGsxKMQfmN2m4Rf/3ZB3F4OYlGvLgssh2IwV\nb9uo72C2OJxqhiu1RS4c06I=\n-----END PRIVATE KEY-----\n";
const clientEmail = "toolebay@atomic-energy-240602.iam.gserviceaccount.com";
const scopes = "https://www.googleapis.com/auth/drive.file";

module.exports = ({ title, name, link }) => {
  return new Promise(resolve => {
    const jwt = new google.auth.JWT(
      clientEmail,
      null,
      privateKey.replace(/\\n/g, "\n"),
      scopes
    );
    jwt.authorize((err, res) => {
      if (err) console.log(err);
      const drive = google.drive({ version: "v3", auth: jwt });
      try {
        let fileMetadata = {
          name,
          parents: ["1_qWjRa9oW8YXW3ZnYNPFvL0_aWBnnfQ_"]
        };
        let media = {
          mimeType: "image/jpeg",
          body: fs.createReadStream(link)
        };
        drive.files.create(
          {
            resource: fileMetadata,
            media: media,
            fields: "id"
          },
          function (err, file) {
            if (err) {
              // Handle error
              console.error(err);
              return resolve({ error: true })
            } else {
              if (fs.exists(link, () => {
                fs.unlink(link, () => { })
              }))
                console.log('====go here')
              return resolve({ errorr: false, title: title, name: name, linkDrive: `https://drive.google.com/open?id=${file.data.id}` })
            }
          }
        );
      } catch (error) {
        return resolve({ error: true })
      }

    });
  });
};
