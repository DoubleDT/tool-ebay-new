const axios = require("axios");
const cheerio = require("cheerio");
const download = require("./download");
let array = [];
module.exports = (link) => {
  return new Promise(resolve => {
    axios.get(link).then(async data => {
      const $ = cheerio.load(data.data);
      let results = $("#ListViewInner").children()

      let listPromises = results.map((x, item) => {
        return array.push(downloadImg(item))
      })
      let resultPromises = await Promise.all(array);
      return resolve(resultPromises.filter(x => !x.error))
    });
  });
  function downloadImg(item) {
    return new Promise(async resolve => {
      let $ = cheerio.load(item)
      let title = $(item)
        .find("h3 > a")
        .text();
      //check title here
      if (title.includes("undefined")) {
        return resolve({ error: true })
      }

      //end
      let img = $(item)
        .find(".lvpic.pic.img.left")
        .find(".lvpicinner.full-width.picW")
        .find("img")
        .attr("src");

      if (img && img.includes("ir.ebaystatic.com")) {
        img = $(item)
          .find(".lvpic.pic.img.left")
          .find(".lvpicinner.full-width.picW")
          .find("img")
          .attr("imgurl");
      }
      if (!img) {
        return resolve({ error: true })
      }

      let resultDownload = await download(img);
      if (resultDownload.error) {
        return resolve({ error: true })
      }
      let { name, link } = resultDownload;
      let data = { title, name, link };
      return resolve(data)
    });
  }
};

